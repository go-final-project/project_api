package main

import (
	"fmt"

	"github.com/gin-gonic/gin"

	"gitlab.com/GoFinalProject/project_api/api"
	"gitlab.com/GoFinalProject/project_api/api/handler"
	"gitlab.com/GoFinalProject/project_api/config"
	"gitlab.com/GoFinalProject/project_api/pkg/logger"
	"gitlab.com/GoFinalProject/project_api/services"
)

func main() {
	cfg := config.Load()

	fmt.Printf("config: %+v/n", cfg)

	// Setup Logger
	loggerLevel := logger.LevelDebug
	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
	case config.TestMode:
		loggerLevel = logger.LevelDebug
	default:
		loggerLevel = logger.LevelInfo
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	grpcSrvc, err := services.NewGrpcClients(cfg)
	if err != nil {
		panic(err)
	}

	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())

	h := handler.NewHandler(cfg, log, grpcSrvc)

	api.SetUpApi(r, h, cfg)

	fmt.Println("Start api gateway...")

	err = r.Run(cfg.HTTPPort)
	if err != nil {
		return
	}
}
