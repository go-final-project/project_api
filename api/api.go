package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.com/GoFinalProject/project_api/api/docs"
	_ "gitlab.com/GoFinalProject/project_api/api/docs"
	"gitlab.com/GoFinalProject/project_api/api/handler"
	"gitlab.com/GoFinalProject/project_api/config"
)

func SetUpApi(r *gin.Engine, h *handler.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(500))

	// Authorization Api
	r.POST("/login", h.Login)
	r.POST("/register", h.Register)

	// Organization Services Api
	// Employee Api
	r.POST("/employee", h.AuthMiddleware(), h.CreateEmployee)
	r.GET("/employee/:id", h.AuthMiddleware(), h.GetByIdEmployee)
	r.GET("employee", h.AuthMiddleware(), h.EmployeeGetList)
	r.PUT("/employee/:id", h.AuthMiddleware(), h.EmployeeUpdate)
	r.DELETE("/employee/:id", h.AuthMiddleware(), h.DeleteEmployee)

	// SalesAddress Api
	r.POST("/sales-address", h.AuthMiddleware(), h.CreateSalesAddress)
	r.GET("/sales-address/:id", h.AuthMiddleware(), h.GetByIdSalesAddress)
	r.GET("sales-address", h.AuthMiddleware(), h.SalesAddressGetList)
	r.PUT("/sales-address/:id", h.AuthMiddleware(), h.SalesAddressUpdate)
	r.DELETE("/sales-address/:id", h.AuthMiddleware(), h.DeleteSalesAddress)

	// Provider Api
	r.POST("/provider", h.AuthMiddleware(), h.CreateProvider)
	r.GET("/provider/:id", h.AuthMiddleware(), h.GetByIdProvider)
	r.GET("provider", h.AuthMiddleware(), h.ProviderGetList)
	r.PUT("/provider/:id", h.AuthMiddleware(), h.ProviderUpdate)
	r.DELETE("/provider/:id", h.AuthMiddleware(), h.DeleteProvider)

	// Branch Api
	r.POST("/branch", h.AuthMiddleware(), h.CreateBranch)
	r.GET("/branch/:id", h.AuthMiddleware(), h.GetByIdBranch)
	r.GET("branch", h.AuthMiddleware(), h.BranchGetList)
	r.PUT("/branch/:id", h.AuthMiddleware(), h.BranchUpdate)
	r.DELETE("/branch/:id", h.AuthMiddleware(), h.DeleteBranch)

	// Product Services Api
	// Product Api
	r.POST("/product", h.AuthMiddleware(), h.CreateProduct)
	r.GET("/product/:id", h.AuthMiddleware(), h.GetByIdProduct)
	r.GET("product", h.AuthMiddleware(), h.ProductGetList)
	r.PUT("/product/:id", h.AuthMiddleware(), h.ProductUpdate)
	r.DELETE("/product/:id", h.AuthMiddleware(), h.DeleteProduct)
	// Category Api
	r.POST("/category", h.AuthMiddleware(), h.CreateCategory)
	r.GET("/category/:id", h.AuthMiddleware(), h.GetByIdCategory)
	r.GET("category", h.AuthMiddleware(), h.CategoryGetList)
	r.PUT("/category/:id", h.AuthMiddleware(), h.CategoryUpdate)
	r.DELETE("/category/:id", h.AuthMiddleware(), h.DeleteCategory)
	// Brand Api
	r.POST("/brand", h.AuthMiddleware(), h.CreateBrand)
	r.GET("/brand/:id", h.AuthMiddleware(), h.GetByIdBrand)
	r.GET("brand", h.AuthMiddleware(), h.BrandGetList)
	r.PUT("/brand/:id", h.AuthMiddleware(), h.BrandUpdate)
	r.DELETE("/brand/:id", h.AuthMiddleware(), h.DeleteBrand)

	// Stock Services Api
	// Coming Api
	r.POST("/coming", h.AuthMiddleware(), h.CreateComing)
	r.GET("/coming/:id", h.AuthMiddleware(), h.GetByIdComing)
	r.GET("coming", h.AuthMiddleware(), h.ComingGetList)
	r.PUT("/coming/:id", h.AuthMiddleware(), h.ComingUpdate)
	r.DELETE("/coming/:id", h.AuthMiddleware(), h.DeleteComing)

	r.POST("/do-prixod", h.AuthMiddleware(), h.DoPrixod)

	// ComingProduct Api
	r.POST("/coming-product", h.AuthMiddleware(), h.CreateComingProduct)
	r.GET("/coming-product/:id", h.AuthMiddleware(), h.GetByIdComingProduct)
	r.GET("coming-product", h.AuthMiddleware(), h.ComingProductGetList)
	r.PUT("/coming-product/:id", h.AuthMiddleware(), h.ComingProductUpdate)
	r.DELETE("/coming-product/:id", h.AuthMiddleware(), h.DeleteComingProduct)

	// Remaining Api
	r.POST("/remaining", h.AuthMiddleware(), h.CreateRemaining)
	r.GET("/remaining/:id", h.AuthMiddleware(), h.GetByIdRemaining)
	r.GET("remaining", h.AuthMiddleware(), h.RemainingGetList)
	r.PUT("/remaining/:id", h.AuthMiddleware(), h.RemainingUpdate)
	r.DELETE("/remaining/:id", h.AuthMiddleware(), h.DeleteRemaining)

	// Kassa Service Api
	// Sale Api
	r.POST("/sale", h.AuthMiddleware(), h.CreateSale)
	r.GET("/sale/:id", h.AuthMiddleware(), h.GetByIdSale)
	r.GET("sale", h.AuthMiddleware(), h.SaleGetList)
	r.PUT("/sale/:id", h.AuthMiddleware(), h.SaleUpdate)
	r.DELETE("/sale/:id", h.AuthMiddleware(), h.DeleteSale)

	r.POST("/make-sale/:id", h.AuthMiddleware(), h.MakeSale)

	// SaleProduct Api
	r.POST("/sale-product", h.AuthMiddleware(), h.CreateSaleProduct)
	r.GET("/sale-product/:id", h.AuthMiddleware(), h.GetByIdSaleProduct)
	r.GET("sale-product", h.AuthMiddleware(), h.SaleProductGetList)
	r.PUT("/sale-product/:id", h.AuthMiddleware(), h.SaleProductUpdate)
	r.DELETE("/sale-product/:id", h.AuthMiddleware(), h.DeleteSaleProduct)

	r.POST("/scan-product", h.AuthMiddleware(), h.ScanProduct)

	// Smena Api
	r.POST("/smena", h.AuthMiddleware(), h.CreateSmena)
	r.GET("/smena/:id", h.AuthMiddleware(), h.GetByIdSmena)
	r.GET("smena", h.AuthMiddleware(), h.SmenaGetList)
	r.PUT("/smena/:id", h.AuthMiddleware(), h.SmenaUpdate)
	r.DELETE("/smena/:id", h.AuthMiddleware(), h.DeleteSmena)

	r.POST("/open-smena", h.AuthMiddleware(), h.OpenSmena)

	r.POST("close-smena/:id", h.AuthMiddleware(), h.CloseSmena)

	// Payment Api
	r.POST("/payment", h.AuthMiddleware(), h.CreatePayment)
	r.GET("/payment/:id", h.AuthMiddleware(), h.GetByIdPayment)
	r.GET("payment", h.AuthMiddleware(), h.PaymentGetList)
	r.PUT("/payment/:id", h.AuthMiddleware(), h.PaymentUpdate)
	r.DELETE("/payment/:id", h.AuthMiddleware(), h.DeletePayment)

	// Transaction Api
	r.POST("/transaction", h.AuthMiddleware(), h.CreateTransaction)
	r.GET("/transaction/:id", h.AuthMiddleware(), h.GetByIdTransaction)
	r.GET("transaction", h.AuthMiddleware(), h.TransactionGetList)
	r.PUT("/transaction/:id", h.AuthMiddleware(), h.TransactionUpdate)
	r.DELETE("/transaction/:id", h.AuthMiddleware(), h.DeleteTransaction)

	url := ginSwagger.URL("swagger/doc.json")
	r.GET("swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
