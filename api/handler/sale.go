package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/kassa_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateSale godoc
// @ID create-sale
// @Router /sale [POST]
// @Summary Create Sale
// @Description Create Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param Sale body kassa_service.SaleCreate true "CreateSaleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateSale(ctx *gin.Context) {
	var Sale kassa_service.SaleCreate

	err := ctx.ShouldBind(&Sale)
	if err != nil {
		h.handlerResponse(ctx, "CreateSale", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleService().Create(ctx, &Sale)
	if err != nil {
		h.handlerResponse(ctx, "SaleService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Sale response", http.StatusOK, resp)
}

// GetByIdSale godoc
// @ID get-by-id_sale
// @Router /sale/{id} [GET]
// @Summary Get By ID Sale
// @Description Get By ID Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdSale(ctx *gin.Context) {
	SaleId := ctx.Param("id")

	if !helper.IsValidUUID(SaleId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.SaleService().GetById(ctx, &kassa_service.SalePrimaryKey{Id: SaleId})
	if err != nil {
		h.handlerResponse(ctx, "SaleService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Sale response", http.StatusCreated, resp)
}

// SaleGetList godoc
// @ID get-list-sale
// @Router /sale [GET]
// @Summary Get List Sale
// @Description Get List Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-branch query string false "search-by-branch"
// @Param search-by-employee query string false "search-by-employee"
// @Param search-by-smena-id query string false "search-by-smena-id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Sale offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Sale limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.SaleService().GetList(ctx.Request.Context(), &kassa_service.SaleGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByBranch:   ctx.Query("search-by-branch"),
		SearchByEmployee: ctx.Query("search-by-employee"),
		SearchBySmenaId:  ctx.Query("search-by-smena-id"),
	})
	if err != nil {
		h.handlerResponse(ctx, "SaleService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Sale response", http.StatusOK, resp)
}

// SaleUpdate godoc
// @ID update-sale
// @Router /sale/{id} [PUT]
// @Summary Update Sale
// @Description Update Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Sale body kassa_service.SaleUpdate true "UpdateSaleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleUpdate(ctx *gin.Context) {
	var (
		id         = ctx.Param("id")
		SaleUpdate kassa_service.SaleUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&SaleUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Sale should bind json", http.StatusBadRequest, err.Error())
		return
	}

	SaleUpdate.Id = id
	resp, err := h.services.SaleService().Update(ctx.Request.Context(), &SaleUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.SaleService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Sale response", http.StatusAccepted, resp)
}

// DeleteSale godoc
// @ID delete-sale
// @Router /sale/{id} [DELETE]
// @Summary Delete Sale
// @Description Delete Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteSale(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.SaleService().Delete(c.Request.Context(), &kassa_service.SalePrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.SaleService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Sale response", http.StatusNoContent, resp)
}
