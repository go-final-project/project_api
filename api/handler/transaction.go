package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/kassa_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateTransaction godoc
// @ID create-transaction
// @Router /transaction [POST]
// @Summary Create Transaction
// @Description Create Transaction
// @Tags Transaction
// @Accept json
// @Procedure json
// @Param Transaction body kassa_service.TransactionCreate true "CreateTransactionRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateTransaction(ctx *gin.Context) {
	var Transaction kassa_service.TransactionCreate

	err := ctx.ShouldBind(&Transaction)
	if err != nil {
		h.handlerResponse(ctx, "CreateTransaction", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.TransactionService().Create(ctx, &Transaction)
	if err != nil {
		h.handlerResponse(ctx, "TransactionService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Transaction response", http.StatusOK, resp)
}

// GetByIdTransaction godoc
// @ID get-by-id_transaction
// @Router /transaction/{id} [GET]
// @Summary Get By ID Transaction
// @Description Get By ID Transaction
// @Tags Transaction
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdTransaction(ctx *gin.Context) {
	TransactionId := ctx.Param("id")

	if !helper.IsValidUUID(TransactionId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.TransactionService().GetById(ctx, &kassa_service.TransactionPrimaryKey{Id: TransactionId})
	if err != nil {
		h.handlerResponse(ctx, "TransactionService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Transaction response", http.StatusCreated, resp)
}

// TransactionGetList godoc
// @ID get-list-transaction
// @Router /transaction [GET]
// @Summary Get List Transaction
// @Description Get List Transaction
// @Tags Transaction
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) TransactionGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Transaction offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Transaction limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.TransactionService().GetList(ctx.Request.Context(), &kassa_service.TransactionGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
	})
	if err != nil {
		h.handlerResponse(ctx, "TransactionService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Transaction response", http.StatusOK, resp)
}

// TransactionUpdate godoc
// @ID update-transaction
// @Router /transaction/{id} [PUT]
// @Summary Update Transaction
// @Description Update Transaction
// @Tags Transaction
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Transaction body kassa_service.TransactionUpdate true "UpdateTransactionRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) TransactionUpdate(ctx *gin.Context) {
	var (
		id                = ctx.Param("id")
		TransactionUpdate kassa_service.TransactionUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&TransactionUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Transaction should bind json", http.StatusBadRequest, err.Error())
		return
	}

	TransactionUpdate.Id = id
	resp, err := h.services.TransactionService().Update(ctx.Request.Context(), &TransactionUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.TransactionService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Transaction response", http.StatusAccepted, resp)
}

// DeleteTransaction godoc
// @ID delete-transaction
// @Router /transaction/{id} [DELETE]
// @Summary Delete Transaction
// @Description Delete Transaction
// @Tags Transaction
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteTransaction(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.TransactionService().Delete(c.Request.Context(), &kassa_service.TransactionPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.TransactionService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Transaction response", http.StatusNoContent, resp)
}
