package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/GoFinalProject/project_api/genproto/kassa_service"
	"gitlab.com/GoFinalProject/project_api/genproto/stock_service"
)

// MakeSale godoc
// @Security ApiKeyAuth
// @ID MakeSale
// @Router /make-sale/{id} [POST]
// @Summary MakeSale
// @Description Make ale
// @Tags MakeSale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h Handler) MakeSale(ctx *gin.Context) {
	MakeSaleId := ctx.Param("id")
	sale, err := h.services.SaleService().GetById(ctx, &kassa_service.SalePrimaryKey{Id: MakeSaleId})
	if err != nil {
		h.handlerResponse(ctx, "SaleService - h.services.SaleService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	_, err = h.services.SaleService().Update(ctx, &kassa_service.SaleUpdate{
		Id:             sale.Id,
		SmenaId:        sale.SmenaId,
		BranchId:       sale.BranchId,
		EmployeeId:     sale.EmployeeId,
		SalesAddressId: sale.SalesAddressId,
		Status:         "finished",
	})
	if err != nil {
		h.handlerResponse(ctx, "SaleService - h.services.SaleService().Update", http.StatusBadRequest, err.Error())
		return
	}
	saleProducts, err := h.services.SaleProductService().GetList(ctx, &kassa_service.SaleProductGetListRequest{
		SearchBySaleId: sale.Id,
	})
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService - h.services.SaleProductService().GetList", http.StatusBadRequest, err.Error())
		return
	}
	for _, saleProduct := range saleProducts.SaleProducts {
		remaining, err := h.services.RemainingService().GetById(ctx, &stock_service.RemainingPrimaryKey{
			ProductName: saleProduct.Id,
		})
		if err != nil {
			h.handlerResponse(ctx, "SaleProductService - h.services.RemainingService().GetById", http.StatusBadRequest, err.Error())
			return
		}
		_, err = h.services.RemainingService().Update(ctx, &stock_service.RemainingUpdate{
			Id:          remaining.Id,
			BranchId:    remaining.BranchId,
			BrandId:     remaining.BrandId,
			CategoryId:  remaining.CategoryId,
			ProductName: remaining.ProductName,
			Barcode:     remaining.Barcode,
			ComingPrice: remaining.ComingPrice,
			Quantity:    remaining.Quantity - saleProduct.Quantity,
		})
		if err != nil {
			h.handlerResponse(ctx, "SaleProductService - h.services.RemainingService().Update", http.StatusBadRequest, err.Error())
			return
		}
	}
	payment, err := h.services.PaymentService().GetById(ctx, &kassa_service.PaymentPrimaryKey{
		Id: sale.Id,
	})
	if err != nil {
		h.handlerResponse(ctx, "PaymentService - h.services.PaymentService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	smena, err := h.services.SmenaService().GetById(ctx, &kassa_service.SmenaPrimaryKey{Id: sale.SmenaId})
	if err != nil {
		h.handlerResponse(ctx, "SmenaService - h.services.SmenaService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	transaction, err := h.services.TransactionService().GetList(ctx, &kassa_service.TransactionGetListRequest{
		SearchBySmenaId: smena.Id,
	})
	if err != nil {
		h.handlerResponse(ctx, "TransactionService - h.services.TransactionService().GetList", http.StatusBadRequest, err.Error())
		return
	}
	_, err = h.services.TransactionService().Update(ctx, &kassa_service.TransactionUpdate{
		Id:         transaction.Transactions[0].Id,
		Cash:       payment.Cash,
		Uzcard:     payment.Uzcard,
		Payme:      payment.Payme,
		Click:      payment.Click,
		Humo:       payment.Humo,
		Apelsin:    payment.Apelsin,
		Visa:       payment.Visa,
		TotalPrice: payment.TotalPrice,
	})
	if err != nil {
		h.handlerResponse(ctx, "TransactionService - h.services.TransactionService().Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "MakeSale Response", http.StatusNoContent, "")
}
