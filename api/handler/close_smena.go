package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/GoFinalProject/project_api/genproto/kassa_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
)

// CloseSmena godoc
// @Security ApiKeyAuth
// @ID CloseSmena
// @Router /close-smena/{id} [POST]
// @Summary CloseSmena
// @Description CloseSmena
// @Tags CloseSmena
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CloseSmena(ctx *gin.Context) {
	SmenaId := ctx.Param("id")
	if !helper.IsValidUUID(SmenaId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}
	resp, err := h.services.SmenaService().GetById(ctx, &kassa_service.SmenaPrimaryKey{Id: SmenaId})
	if err != nil {
		h.handlerResponse(ctx, "CloseSmena - h.services.SmenaService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	_, err = h.services.SmenaService().Update(ctx, &kassa_service.SmenaUpdate{
		Id:             resp.Id,
		BranchId:       resp.BranchId,
		EmployeeId:     resp.EmployeeId,
		SalesAddressId: resp.SalesAddressId,
		Status:         "closed",
	})
	if err != nil {
		h.handlerResponse(ctx, "CloseSmena - h.services.h.services.SmenaService().Update", http.StatusBadRequest, err.Error())
		return
	}
	h.handlerResponse(ctx, "Close Smena response", http.StatusNoContent, "")
}
