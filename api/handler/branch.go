package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/organization_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateBranch godoc
// @ID create-branch
// @Router /branch [POST]
// @Summary Create Branch
// @Description Create Branch
// @Tags Branch
// @Accept json
// @Procedure json
// @Param Branch body organization_service.BranchCreate true "CreateBranchRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateBranch(ctx *gin.Context) {
	var Branch organization_service.BranchCreate

	err := ctx.ShouldBind(&Branch)
	if err != nil {
		h.handlerResponse(ctx, "CreateBranch", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BranchService().Create(ctx, &Branch)
	if err != nil {
		h.handlerResponse(ctx, "BranchService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Branch response", http.StatusOK, resp)
}

// GetByIdBranch godoc
// @ID get-by-id_branch
// @Router /branch/{id} [GET]
// @Summary Get By ID Branch
// @Description Get By ID Branch
// @Tags Branch
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdBranch(ctx *gin.Context) {
	BranchId := ctx.Param("id")

	if !helper.IsValidUUID(BranchId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.BranchService().GetById(ctx, &organization_service.BranchPrimaryKey{Id: BranchId})
	if err != nil {
		h.handlerResponse(ctx, "BranchService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Branch response", http.StatusCreated, resp)
}

// BranchGetList godoc
// @ID get-list-branch
// @Router /branch [GET]
// @Summary Get List Branch
// @Description Get List Branch
// @Tags Branch
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-name query string false "search-by-name"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) BranchGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Branch offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Branch limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.BranchService().GetList(ctx.Request.Context(), &organization_service.BranchGetListRequest{
		Offset:       int64(offset),
		Limit:        int64(limit),
		SearchByName: ctx.Query("search-by-name"),
	})
	if err != nil {
		h.handlerResponse(ctx, "BranchService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Branch response", http.StatusOK, resp)
}

// BranchUpdate godoc
// @ID update-branch
// @Router /branch/{id} [PUT]
// @Summary Update Branch
// @Description Update Branch
// @Tags Branch
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Branch body organization_service.BranchUpdate true "UpdateBranchRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) BranchUpdate(ctx *gin.Context) {
	var (
		id           = ctx.Param("id")
		BranchUpdate organization_service.BranchUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&BranchUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Branch should bind json", http.StatusBadRequest, err.Error())
		return
	}

	BranchUpdate.Id = id
	resp, err := h.services.BranchService().Update(ctx.Request.Context(), &BranchUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.BranchService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Branch response", http.StatusAccepted, resp)
}

// DeleteBranch godoc
// @ID delete-branch
// @Router /branch/{id} [DELETE]
// @Summary Delete Branch
// @Description Delete Branch
// @Tags Branch
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteBranch(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.BranchService().Delete(c.Request.Context(), &organization_service.BranchPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.BranchService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Branch response", http.StatusNoContent, resp)
}
