package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/organization_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateEmployee godoc
// @ID create-employee
// @Router /employee [POST]
// @Summary Create Employee
// @Description Create Employee
// @Tags Employee
// @Accept json
// @Procedure json
// @Param Employee body organization_service.EmployeeCreate true "CreateEmployeeRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateEmployee(ctx *gin.Context) {
	var Employee organization_service.EmployeeCreate

	err := ctx.ShouldBind(&Employee)
	if err != nil {
		h.handlerResponse(ctx, "CreateEmployee", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.EmployeeService().Create(ctx, &Employee)
	if err != nil {
		h.handlerResponse(ctx, "EmployeeService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Employee response", http.StatusOK, resp)
}

// GetByIdEmployee godoc
// @ID get-by-id-employee
// @Router /employee/{id} [GET]
// @Summary Get By ID Employee
// @Description Get By ID Employee
// @Tags Employee
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdEmployee(ctx *gin.Context) {
	EmployeeId := ctx.Param("id")

	if !helper.IsValidUUID(EmployeeId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.EmployeeService().GetById(ctx, &organization_service.EmployeePrimaryKey{Id: EmployeeId})
	if err != nil {
		h.handlerResponse(ctx, "EmployeeService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Employee response", http.StatusCreated, resp)
}

// EmployeeGetList godoc
// @ID get-list-employee
// @Router /employee [GET]
// @Summary Get List Employee
// @Description Get List Employee
// @Tags Employee
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-first-name query string false "search-by-first-name"
// @Param search-by-last-name query string false "search-by-last-name"
// @Param search-by-phone-number query string false "search-by-phone-number"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) EmployeeGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Employee offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Employee limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.EmployeeService().GetList(ctx.Request.Context(), &organization_service.EmployeeGetListRequest{
		Offset:              int64(offset),
		Limit:               int64(limit),
		SearchByFirstName:   ctx.Query("search-by-first-name"),
		SearchByLastName:    ctx.Query("search-by-last-name"),
		SearchByPhoneNumber: ctx.Query("search-by-phone-number"),
	})
	if err != nil {
		h.handlerResponse(ctx, "EmployeeService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Employee response", http.StatusOK, resp)
}

// EmployeeUpdate godoc
// @ID update-employee
// @Router /employee/{id} [PUT]
// @Summary Update Employee
// @Description Update Employee
// @Tags Employee
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Employee body organization_service.EmployeeUpdate true "UpdateEmployeeRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) EmployeeUpdate(ctx *gin.Context) {
	var (
		id             = ctx.Param("id")
		EmployeeUpdate organization_service.EmployeeUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&EmployeeUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Employee should bind json", http.StatusBadRequest, err.Error())
		return
	}

	EmployeeUpdate.Id = id
	resp, err := h.services.EmployeeService().Update(ctx.Request.Context(), &EmployeeUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.EmployeeService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Employee response", http.StatusAccepted, resp)
}

// DeleteEmployee godoc
// @ID delete-employee
// @Router /employee/{id} [DELETE]
// @Summary Delete Employee
// @Description Delete Employee
// @Tags Employee
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteEmployee(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.EmployeeService().Delete(c.Request.Context(), &organization_service.EmployeePrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.EmployeeService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Employee response", http.StatusNoContent, resp)
}
