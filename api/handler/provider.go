package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/organization_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateProvider godoc
// @ID create-provider
// @Router /provider [POST]
// @Summary Create Provider
// @Description Create Provider
// @Tags Provider
// @Accept json
// @Procedure json
// @Param Provider body organization_service.ProviderCreate true "CreateProviderRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateProvider(ctx *gin.Context) {
	var Provider organization_service.ProviderCreate

	err := ctx.ShouldBind(&Provider)
	if err != nil {
		h.handlerResponse(ctx, "CreateProvider", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ProviderService().Create(ctx, &Provider)
	if err != nil {
		h.handlerResponse(ctx, "ProviderService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Provider response", http.StatusOK, resp)
}

// GetByIdProvider godoc
// @ID get-by-id-provider
// @Router /provider/{id} [GET]
// @Summary Get By ID Provider
// @Description Get By ID Provider
// @Tags Provider
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdProvider(ctx *gin.Context) {
	ProviderId := ctx.Param("id")

	if !helper.IsValidUUID(ProviderId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.ProviderService().GetById(ctx, &organization_service.ProviderPrimaryKey{Id: ProviderId})
	if err != nil {
		h.handlerResponse(ctx, "ProviderService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Provider response", http.StatusCreated, resp)
}

// ProviderGetList godoc
// @ID get-list-provider
// @Router /provider [GET]
// @Summary Get List Provider
// @Description Get List Provider
// @Tags Provider
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-name query string false "search-by-name"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ProviderGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Provider offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Provider limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.ProviderService().GetList(ctx.Request.Context(), &organization_service.ProviderGetListRequest{
		Offset:       int64(offset),
		Limit:        int64(limit),
		SearchByName: ctx.Query("search-by-name"),
	})
	if err != nil {
		h.handlerResponse(ctx, "ProviderService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Provider response", http.StatusOK, resp)
}

// ProviderUpdate godoc
// @ID update-provider
// @Router /provider/{id} [PUT]
// @Summary Update Provider
// @Description Update Provider
// @Tags Provider
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Provider body organization_service.ProviderUpdate true "UpdateProviderRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ProviderUpdate(ctx *gin.Context) {
	var (
		id             = ctx.Param("id")
		ProviderUpdate organization_service.ProviderUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&ProviderUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Provider should bind json", http.StatusBadRequest, err.Error())
		return
	}

	ProviderUpdate.Id = id
	resp, err := h.services.ProviderService().Update(ctx.Request.Context(), &ProviderUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.ProviderService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Provider response", http.StatusAccepted, resp)
}

// DeleteProvider godoc
// @ID delete-provider
// @Router /provider/{id} [DELETE]
// @Summary Delete Provider
// @Description Delete Provider
// @Tags Provider
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteProvider(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.ProviderService().Delete(c.Request.Context(), &organization_service.ProviderPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.ProviderService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Provider response", http.StatusNoContent, resp)
}
