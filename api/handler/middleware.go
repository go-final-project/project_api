package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
)

func (h *Handler) AuthMiddleware() gin.HandlerFunc {

	return func(c *gin.Context) {

		value := c.GetHeader("Authorization")

		info, err := helper.ParseClaims(value, h.cfg.SecretKey)

		if err != nil {
			c.AbortWithError(http.StatusForbidden, err)
			return
		}

		c.Set("Auth", info)
		c.Next()
	}
}
