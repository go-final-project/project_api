package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/product_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateProduct godoc
// @ID create-product
// @Router /product [POST]
// @Summary Create Product
// @Description Create Product
// @Tags Product
// @Accept json
// @Procedure json
// @Param Product body product_service.ProductCreate true "CreateProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateProduct(ctx *gin.Context) {
	var product product_service.ProductCreate

	err := ctx.ShouldBind(&product)
	if err != nil {
		h.handlerResponse(ctx, "CreateProduct", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ProductService().Create(ctx, &product)
	if err != nil {
		h.handlerResponse(ctx, "ProductService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Product response", http.StatusCreated, resp)
}

// GetByIdProduct godoc
// @ID get-by-id-product
// @Router /product/{id} [GET]
// @Summary Get By ID Product
// @Description Get By ID Product
// @Tags Product
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdProduct(ctx *gin.Context) {
	productId := ctx.Param("id")

	if !helper.IsValidUUID(productId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.ProductService().GetById(ctx, &product_service.ProductPrimaryKey{Id: productId})
	if err != nil {
		h.handlerResponse(ctx, "ProductService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Product response", http.StatusOK, resp)
}

// ProductGetList godoc
// @ID get-list-product
// @Router /product [GET]
// @Summary Get List Product
// @Description Get List Product
// @Tags Product
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-name query string false "search-by-name"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ProductGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Product offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Product limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.ProductService().GetList(ctx.Request.Context(), &product_service.ProductGetListRequest{
		Offset:       int64(offset),
		Limit:        int64(limit),
		SearchByName: ctx.Query("search-by-name"),
	})
	if err != nil {
		h.handlerResponse(ctx, "ProductService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Product response", http.StatusOK, resp)
}

// ProductUpdate godoc
// @ID update-product
// @Router /product/{id} [PUT]
// @Summary Update Product
// @Description Update Product
// @Tags Product
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Product body product_service.ProductUpdate true "UpdateProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ProductUpdate(ctx *gin.Context) {
	var (
		id            = ctx.Param("id")
		ProductUpdate product_service.ProductUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&ProductUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Product should bind json", http.StatusBadRequest, err.Error())
		return
	}

	ProductUpdate.Id = id
	resp, err := h.services.ProductService().Update(ctx.Request.Context(), &ProductUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.ProductService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Product response", http.StatusAccepted, resp)
}

// DeleteProduct godoc
// @ID delete-product
// @Router /product/{id} [DELETE]
// @Summary Delete Product
// @Description Delete Product
// @Tags Product
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteProduct(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.ProductService().Delete(c.Request.Context(), &product_service.ProductPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.ProductService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Product response", http.StatusNoContent, resp)
}
