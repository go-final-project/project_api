package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/stock_service"
	"net/http"

	"github.com/gin-gonic/gin"
)

// DoPrixod godoc
// @ID create-do-prixod
// @Router /do-prixod [POST]
// @Summary Create DoPrixod
// @Description Create DoPrixod
// @Tags DoPrixod
// @Accept json
// @Procedure json
// @Param coming-id query string false "coming-id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DoPrixod(ctx *gin.Context) {
	var ComingId = ctx.Query("coming-id")

	resp, err := h.services.ComingProductService().GetList(ctx, &stock_service.ComingProductGetListRequest{
		SearchByComingId: ComingId,
	})
	if err != nil {
		h.handlerResponse(ctx, "ComingProductService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	coming, err := h.services.ComingService().GetById(ctx, &stock_service.ComingPrimaryKey{Id: ComingId})
	if err != nil {
		h.handlerResponse(ctx, "ComingService().GetById", http.StatusInternalServerError, err.Error())
		return
	}

	for _, product := range resp.ComingProducts {
		remaining, err := h.services.RemainingService().GetById(ctx, &stock_service.RemainingPrimaryKey{Id: product.Id})
		if err != nil {
			create, err := h.services.RemainingService().Create(ctx, &stock_service.RemainingCreate{
				BranchId:    coming.BranchId,
				BrandId:     product.BrandId,
				CategoryId:  product.CategoryId,
				ProductName: product.ProductName,
				Barcode:     product.Barcode,
				ComingPrice: product.ComingPrice,
				Quantity:    product.Quantity,
			})
			if err != nil {
				h.handlerResponse(ctx, "RemainingService().Create", http.StatusInternalServerError, err.Error())
				return
			}
			h.handlerResponse(ctx, "RemainingService().Create", http.StatusCreated, create)
		} else {
			update, err := h.services.RemainingService().Update(ctx, &stock_service.RemainingUpdate{
				Id:          product.Id,
				BranchId:    coming.BranchId,
				BrandId:     product.BrandId,
				CategoryId:  product.CategoryId,
				ProductName: product.ProductName,
				Barcode:     product.Barcode,
				ComingPrice: product.ComingPrice,
				Quantity:    remaining.Quantity + product.Quantity,
			})
			if err != nil {
				h.handlerResponse(ctx, "RemainingService().Update", http.StatusInternalServerError, err.Error())
				return
			}
			h.handlerResponse(ctx, "RemainingService().Update", http.StatusOK, update)
		}
	}

	update, err := h.services.ComingService().Update(ctx, &stock_service.ComingUpdate{
		Id:     ComingId,
		Status: "finished",
	})
	if err != nil {
		h.handlerResponse(ctx, "ComingService().Update", http.StatusInternalServerError, err.Error())
		return
	}
	h.handlerResponse(ctx, "ComingService().Update", http.StatusOK, update)
}
