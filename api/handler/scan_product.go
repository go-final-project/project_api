package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/GoFinalProject/project_api/genproto/kassa_service"
	"gitlab.com/GoFinalProject/project_api/genproto/product_service"
	"gitlab.com/GoFinalProject/project_api/genproto/stock_service"
)

// ScanProduct godoc
// @Security ApiKeyAuth
// @ID ScanProduct
// @Router /scan-product [POST]
// @Summary ScanProduct
// @Description ScanProduct
// @Tags ScanProduct
// @Accept json
// @Procedure json
// @Param ScanProduct body kassa_service.ScanProduct true "MakeIncomeRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ScanProduct(ctx *gin.Context) {
	var ScanProduct kassa_service.ScanProduct

	err := ctx.ShouldBind(&ScanProduct)
	if err != nil {
		h.handlerResponse(ctx, "ScanProduct", http.StatusBadRequest, err.Error())
		return
	}
	saleProduct, err := h.services.SaleProductService().GetById(ctx, &kassa_service.SaleProductPrimaryKey{
		Barcode: ScanProduct.Barcode,
	})
	if err != nil {
		if err.Error() == "no rows in result set" {
			product, err := h.services.ProductService().GetList(ctx, &product_service.ProductGetListRequest{
				SearchByBarcode: ScanProduct.Barcode,
			})
			if err != nil {
				h.handlerResponse(ctx, "ScanProduct - h.services.ProductService().GetList", http.StatusBadRequest, err.Error())
				return
			}
			remaining, err := h.services.RemainingService().GetById(ctx, &stock_service.RemainingPrimaryKey{
				ProductName: product.Products[0].Id,
			})
			if err != nil {
				h.handlerResponse(ctx, "ScanProduct - h.services.RemainingService().GetById", http.StatusBadRequest, err.Error())
				return
			}
			resp, err := h.services.SaleProductService().Create(ctx, &kassa_service.SaleProductCreate{
				SaleId:            ScanProduct.SaleId,
				BrandId:           remaining.BrandId,
				CategoryId:        remaining.CategoryId,
				ProductName:       remaining.ProductName,
				Barcode:           remaining.Barcode,
				RemainingQuantity: remaining.Quantity,
				Quantity:          1,
				Price:             remaining.ComingPrice,
				TotalPrice:        remaining.ComingPrice,
			})
			if err != nil {
				h.handlerResponse(ctx, "ScanProduct - h.services.SaleProductService().Create", http.StatusBadRequest, err.Error())
				return
			}
			h.handlerResponse(ctx, "Scan Product response", http.StatusNoContent, resp)
			return
		} else {
			h.handlerResponse(ctx, "ScanProduct - SaleProductService().GetById", http.StatusBadRequest, err.Error())
			return
		}
	}

	resp, err := h.services.SaleProductService().Update(ctx, &kassa_service.SaleProductUpdate{
		Id:                saleProduct.Id,
		SaleId:            saleProduct.SaleId,
		BrandId:           saleProduct.BrandId,
		CategoryId:        saleProduct.CategoryId,
		ProductName:       saleProduct.ProductName,
		Barcode:           saleProduct.Barcode,
		RemainingQuantity: saleProduct.RemainingQuantity,
		Quantity:          saleProduct.Quantity + 1,
		Price:             saleProduct.Price,
		TotalPrice:        saleProduct.TotalPrice,
	})
	if err != nil {
		h.handlerResponse(ctx, "ScanProduct - SaleProductService().Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "Scan Product response", http.StatusNoContent, resp)
}
