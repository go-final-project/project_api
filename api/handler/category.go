package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/product_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateCategory godoc
// @ID create-category
// @Router /category [POST]
// @Summary Create Category
// @Description Create Category
// @Tags Category
// @Accept json
// @Procedure json
// @Param Category body product_service.CategoryCreate true "CreateCategoryRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateCategory(ctx *gin.Context) {
	var category product_service.CategoryCreate

	err := ctx.ShouldBind(&category)
	if err != nil {
		h.handlerResponse(ctx, "CreateCategory", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.CategoryService().Create(ctx, &category)
	if err != nil {
		h.handlerResponse(ctx, "CategoryService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Category response", http.StatusCreated, resp)
}

// GetByIdCategory godoc
// @ID get-by-id-category
// @Router /category/{id} [GET]
// @Summary Get By ID Category
// @Description Get By ID Category
// @Tags Category
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdCategory(ctx *gin.Context) {
	categoryId := ctx.Param("id")

	if !helper.IsValidUUID(categoryId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.CategoryService().GetById(ctx, &product_service.CategoryPrimaryKey{Id: categoryId})
	if err != nil {
		h.handlerResponse(ctx, "CategoryService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Category response", http.StatusOK, resp)
}

// CategoryGetList godoc
// @ID get-list-category
// @Router /category [GET]
// @Summary Get List Category
// @Description Get List Category
// @Tags Category
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-name query string false "search-by-name"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CategoryGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Category offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Category limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.CategoryService().GetList(ctx.Request.Context(), &product_service.CategoryGetListRequest{
		Offset:       int64(offset),
		Limit:        int64(limit),
		SearchByName: ctx.Query("search-by-name"),
	})
	if err != nil {
		h.handlerResponse(ctx, "CategoryService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Category response", http.StatusOK, resp)
}

// CategoryUpdate godoc
// @ID update-category
// @Router /category/{id} [PUT]
// @Summary Update Category
// @Description Update Category
// @Tags Category
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Category body product_service.CategoryUpdate true "UpdateCategoryRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CategoryUpdate(ctx *gin.Context) {
	var (
		id             = ctx.Param("id")
		CategoryUpdate product_service.CategoryUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&CategoryUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Category should bind json", http.StatusBadRequest, err.Error())
		return
	}

	CategoryUpdate.Id = id
	resp, err := h.services.CategoryService().Update(ctx.Request.Context(), &CategoryUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.CategoryService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Category response", http.StatusAccepted, resp)
}

// DeleteCategory godoc
// @ID delete-category
// @Router /category/{id} [DELETE]
// @Summary Delete Category
// @Description Delete Category
// @Tags Category
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteCategory(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.CategoryService().Delete(c.Request.Context(), &product_service.CategoryPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.CategoryService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Category response", http.StatusNoContent, resp)
}
