package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/organization_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateSalesAddress godoc
// @ID create-sales-address
// @Router /sales-address [POST]
// @Summary Create SalesAddress
// @Description Create SalesAddress
// @Tags SalesAddress
// @Accept json
// @Procedure json
// @Param SalesAddress body organization_service.SalesAddressCreate true "CreateSalesAddressRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateSalesAddress(ctx *gin.Context) {
	var SalesAddress organization_service.SalesAddressCreate

	err := ctx.ShouldBind(&SalesAddress)
	if err != nil {
		h.handlerResponse(ctx, "CreateSalesAddress", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SalesAddressService().Create(ctx, &SalesAddress)
	if err != nil {
		h.handlerResponse(ctx, "SalesAddressService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create SalesAddress response", http.StatusOK, resp)
}

// GetByIdSalesAddress godoc
// @ID get-by-id-sales-address
// @Router /sales-address/{id} [GET]
// @Summary Get By ID SalesAddress
// @Description Get By ID SalesAddress
// @Tags SalesAddress
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdSalesAddress(ctx *gin.Context) {
	SalesAddressId := ctx.Param("id")

	if !helper.IsValidUUID(SalesAddressId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.SalesAddressService().GetById(ctx, &organization_service.SalesAddressPrimaryKey{Id: SalesAddressId})
	if err != nil {
		h.handlerResponse(ctx, "SalesAddressService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id SalesAddress response", http.StatusCreated, resp)
}

// SalesAddressGetList godoc
// @ID get-list-sales-address
// @Router /sales-address [GET]
// @Summary Get List SalesAddress
// @Description Get List SalesAddress
// @Tags SalesAddress
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-name query string false "search-by-name"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SalesAddressGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list SalesAddress offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list SalesAddress limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.SalesAddressService().GetList(ctx.Request.Context(), &organization_service.SalesAddressGetListRequest{
		Offset:       int64(offset),
		Limit:        int64(limit),
		SearchByName: ctx.Query("search-by-name"),
	})
	if err != nil {
		h.handlerResponse(ctx, "SalesAddressService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list SalesAddress response", http.StatusOK, resp)
}

// SalesAddressUpdate godoc
// @ID update-sales-address
// @Router /sales-address/{id} [PUT]
// @Summary Update SalesAddress
// @Description Update SalesAddress
// @Tags SalesAddress
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param SalesAddress body organization_service.SalesAddressUpdate true "UpdateSalesAddressRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SalesAddressUpdate(ctx *gin.Context) {
	var (
		id                 = ctx.Param("id")
		SalesAddressUpdate organization_service.SalesAddressUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&SalesAddressUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error SalesAddress should bind json", http.StatusBadRequest, err.Error())
		return
	}

	SalesAddressUpdate.Id = id
	resp, err := h.services.SalesAddressService().Update(ctx.Request.Context(), &SalesAddressUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.SalesAddressService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create SalesAddress response", http.StatusAccepted, resp)
}

// DeleteSalesAddress godoc
// @ID delete-sales-address
// @Router /sales-address/{id} [DELETE]
// @Summary Delete SalesAddress
// @Description Delete SalesAddress
// @Tags SalesAddress
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteSalesAddress(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.SalesAddressService().Delete(c.Request.Context(), &organization_service.SalesAddressPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.SalesAddressService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create SalesAddress response", http.StatusNoContent, resp)
}
