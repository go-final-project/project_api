package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/stock_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateRemaining godoc
// @ID create-remaining
// @Router /remaining [POST]
// @Summary Create Remaining
// @Description Create Remaining
// @Tags Remaining
// @Accept json
// @Procedure json
// @Param Remaining body stock_service.RemainingCreate true "CreateRemainingRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateRemaining(ctx *gin.Context) {
	var remaining stock_service.RemainingCreate

	err := ctx.ShouldBind(&remaining)
	if err != nil {
		h.handlerResponse(ctx, "CreateRemaining", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.RemainingService().Create(ctx, &remaining)
	if err != nil {
		h.handlerResponse(ctx, "RemainingService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Remaining response", http.StatusCreated, resp)
}

// GetByIdRemaining godoc
// @ID get-by-id-remaining
// @Router /remaining/{id} [GET]
// @Summary Get By ID Remaining
// @Description Get By ID Remaining
// @Tags Remaining
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdRemaining(ctx *gin.Context) {
	remainingId := ctx.Param("id")

	if !helper.IsValidUUID(remainingId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.RemainingService().GetById(ctx, &stock_service.RemainingPrimaryKey{Id: remainingId})
	if err != nil {
		h.handlerResponse(ctx, "RemainingService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Remaining response", http.StatusOK, resp)
}

// RemainingGetList godoc
// @ID get-list-remaining
// @Router /remaining [GET]
// @Summary Get List Remaining
// @Description Get List Remaining
// @Tags Remaining
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-category query string false "search-by-category"
// @Param search-by-barcode query string false "search-by-barcode"
// @Param search-by-brand query string false "search-by-brand"
// @Param search-by-branch query string false "search-by-branch"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) RemainingGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Remaining offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Remaining limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.RemainingService().GetList(ctx.Request.Context(), &stock_service.RemainingGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByCategory: ctx.Query("search-by-category"),
		SearchByBarcode:  ctx.Query("search-by-barcode"),
		SearchByBrand:    ctx.Query("search-by-brand"),
		SearchByBranch:   ctx.Query("search-by-branch"),
	})
	if err != nil {
		h.handlerResponse(ctx, "RemainingService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Remaining response", http.StatusOK, resp)
}

// RemainingUpdate godoc
// @ID update-remaining
// @Router /remaining/{id} [PUT]
// @Summary Update Remaining
// @Description Update Remaining
// @Tags Remaining
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Remaining body stock_service.RemainingUpdate true "UpdateRemainingRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) RemainingUpdate(ctx *gin.Context) {
	var (
		id              = ctx.Param("id")
		RemainingUpdate stock_service.RemainingUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&RemainingUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Remaining should bind json", http.StatusBadRequest, err.Error())
		return
	}

	RemainingUpdate.Id = id
	resp, err := h.services.RemainingService().Update(ctx.Request.Context(), &RemainingUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.RemainingService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Remaining response", http.StatusAccepted, resp)
}

// DeleteRemaining godoc
// @ID delete-remaining
// @Router /remaining/{id} [DELETE]
// @Summary Delete Remaining
// @Description Delete Remaining
// @Tags Remaining
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteRemaining(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.RemainingService().Delete(c.Request.Context(), &stock_service.RemainingPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.RemainingService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Remaining response", http.StatusNoContent, resp)
}
