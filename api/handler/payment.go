package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/kassa_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreatePayment godoc
// @ID create-payment
// @Router /payment [POST]
// @Summary Create Payment
// @Description Create Payment
// @Tags Payment
// @Accept json
// @Procedure json
// @Param Payment body kassa_service.PaymentCreate true "CreatePaymentRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreatePayment(ctx *gin.Context) {
	var Payment kassa_service.PaymentCreate

	err := ctx.ShouldBind(&Payment)
	if err != nil {
		h.handlerResponse(ctx, "CreatePayment", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.PaymentService().Create(ctx, &Payment)
	if err != nil {
		h.handlerResponse(ctx, "PaymentService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Payment response", http.StatusOK, resp)
}

// GetByIdPayment godoc
// @ID get-by-id_payment
// @Router /payment/{id} [GET]
// @Summary Get By ID Payment
// @Description Get By ID Payment
// @Tags Payment
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdPayment(ctx *gin.Context) {
	PaymentId := ctx.Param("id")

	if !helper.IsValidUUID(PaymentId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.PaymentService().GetById(ctx, &kassa_service.PaymentPrimaryKey{Id: PaymentId})
	if err != nil {
		h.handlerResponse(ctx, "PaymentService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Payment response", http.StatusCreated, resp)
}

// PaymentGetList godoc
// @ID get-list-payment
// @Router /payment [GET]
// @Summary Get List Payment
// @Description Get List Payment
// @Tags Payment
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) PaymentGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Payment offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Payment limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.PaymentService().GetList(ctx.Request.Context(), &kassa_service.PaymentGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
	})
	if err != nil {
		h.handlerResponse(ctx, "PaymentService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Payment response", http.StatusOK, resp)
}

// PaymentUpdate godoc
// @ID update-payment
// @Router /payment/{id} [PUT]
// @Summary Update Payment
// @Description Update Payment
// @Tags Payment
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Payment body kassa_service.PaymentUpdate true "UpdatePaymentRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) PaymentUpdate(ctx *gin.Context) {
	var (
		id            = ctx.Param("id")
		PaymentUpdate kassa_service.PaymentUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&PaymentUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Payment should bind json", http.StatusBadRequest, err.Error())
		return
	}

	PaymentUpdate.Id = id
	resp, err := h.services.PaymentService().Update(ctx.Request.Context(), &PaymentUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.PaymentService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Payment response", http.StatusAccepted, resp)
}

// DeletePayment godoc
// @ID delete-payment
// @Router /payment/{id} [DELETE]
// @Summary Delete Payment
// @Description Delete Payment
// @Tags Payment
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeletePayment(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.PaymentService().Delete(c.Request.Context(), &kassa_service.PaymentPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.PaymentService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Payment response", http.StatusNoContent, resp)
}
