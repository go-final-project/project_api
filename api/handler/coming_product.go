package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/stock_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateComingProduct godoc
// @ID create-coming-product
// @Router /coming-product [POST]
// @Summary Create ComingProduct
// @Description Create ComingProduct
// @Tags ComingProduct
// @Accept json
// @Procedure json
// @Param ComingProduct body stock_service.ComingProductCreate true "CreateComingProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateComingProduct(ctx *gin.Context) {
	var comingProduct stock_service.ComingProductCreate

	err := ctx.ShouldBind(&comingProduct)
	if err != nil {
		h.handlerResponse(ctx, "CreateComingProduct", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ComingProductService().Create(ctx, &comingProduct)
	if err != nil {
		h.handlerResponse(ctx, "ComingProductService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create ComingProduct response", http.StatusCreated, resp)
}

// GetByIdComingProduct godoc
// @ID get-by-id-coming-product
// @Router /coming-product/{id} [GET]
// @Summary Get By ID ComingProduct
// @Description Get By ID ComingProduct
// @Tags ComingProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdComingProduct(ctx *gin.Context) {
	comingProductId := ctx.Param("id")

	if !helper.IsValidUUID(comingProductId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.ComingProductService().GetById(ctx, &stock_service.ComingProductPrimaryKey{Id: comingProductId})
	if err != nil {
		h.handlerResponse(ctx, "ComingProductService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id ComingProduct response", http.StatusOK, resp)
}

// ComingProductGetList godoc
// @ID get-list-coming-product
// @Router /coming-product [GET]
// @Summary Get List ComingProduct
// @Description Get List ComingProduct
// @Tags ComingProduct
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-category query string false "search-by-category"
// @Param search-by-barcode query string false "search-by-barcode"
// @Param search-by-brand query string false "search-by-brand"
// @Param search-by-coming-id query string false "search-by-coming-id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ComingProductGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list ComingProduct offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list ComingProduct limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.ComingProductService().GetList(ctx.Request.Context(), &stock_service.ComingProductGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByCategory: ctx.Query("search-by-category"),
		SearchByBarcode:  ctx.Query("search-by-barcode"),
		SearchByBrand:    ctx.Query("search-by-brand"),
		SearchByComingId: ctx.Query("search-by-coming-id"),
	})
	if err != nil {
		h.handlerResponse(ctx, "ComingProductService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list ComingProduct response", http.StatusOK, resp)
}

// ComingProductUpdate godoc
// @ID update-coming-product
// @Router /coming-product/{id} [PUT]
// @Summary Update ComingProduct
// @Description Update ComingProduct
// @Tags ComingProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param ComingProduct body stock_service.ComingProductUpdate true "UpdateComingProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ComingProductUpdate(ctx *gin.Context) {
	var (
		id                  = ctx.Param("id")
		ComingProductUpdate stock_service.ComingProductUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&ComingProductUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error ComingProduct should bind json", http.StatusBadRequest, err.Error())
		return
	}

	ComingProductUpdate.Id = id
	resp, err := h.services.ComingProductService().Update(ctx.Request.Context(), &ComingProductUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.ComingProductService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create ComingProduct response", http.StatusAccepted, resp)
}

// DeleteComingProduct godoc
// @ID delete-coming-product
// @Router /coming-product/{id} [DELETE]
// @Summary Delete ComingProduct
// @Description Delete ComingProduct
// @Tags ComingProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteComingProduct(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.ComingProductService().Delete(c.Request.Context(), &stock_service.ComingProductPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.ComingProductService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create ComingProduct response", http.StatusNoContent, resp)
}
