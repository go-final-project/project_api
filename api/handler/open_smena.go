package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/GoFinalProject/project_api/genproto/kassa_service"
)

// OpenSmena godoc
// @Security ApiKeyAuth
// @ID OpenSmena
// @Router /open-smena [POST]
// @Summary OpenSmena
// @Description OpenSmena
// @Tags OpenSmena
// @Accept json
// @Procedure json
// @Param OpenSmena body kassa_service.SmenaCreate true "OpenSmenaRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) OpenSmena(ctx *gin.Context) {
	var Smena kassa_service.SmenaCreate
	err := ctx.ShouldBind(&Smena)
	if err != nil {
		h.handlerResponse(ctx, "OpenSmena", http.StatusBadRequest, err.Error())
		return
	}
	branch, err := h.services.SmenaService().GetList(ctx, &kassa_service.SmenaGetListRequest{
		SearchByBranch: Smena.BranchId,
	})
	if err != nil {
		h.handlerResponse(ctx, "OpenSmena - h.services.SmenaService().GetList", http.StatusBadRequest, err.Error())
		return
	}
	for _, value := range branch.Smena {
		if value.Status == "opened" {
			h.handlerResponse(ctx, "Found Opened Smena", http.StatusBadRequest, "У этого филиала уже есть открытая смена. Пожалуйста, сначала закройте его")
			return
		}
	}
	resp, err := h.services.SmenaService().Create(ctx, &Smena)
	if err != nil {
		h.handlerResponse(ctx, "OpenSmena - h.services.SmenaService().Create", http.StatusBadRequest, err.Error())
		return
	}
	_, err = h.services.TransactionService().Create(ctx, &kassa_service.TransactionCreate{
		Cash:       0,
		Uzcard:     0,
		Payme:      0,
		Click:      0,
		Humo:       0,
		Apelsin:    0,
		Visa:       0,
		TotalPrice: 0,
		SmenaId:    resp.Id,
	})
	if err != nil {
		h.handlerResponse(ctx, "OpenSmena - h.services.TransactionService().Create", http.StatusBadRequest, err.Error())
		return
	}
	h.handlerResponse(ctx, "OpenSmena response", http.StatusOK, resp)
}
