package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/kassa_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateSmena godoc
// @ID create-smena
// @Router /smena [POST]
// @Summary Create Smena
// @Description Create Smena
// @Tags Smena
// @Accept json
// @Procedure json
// @Param Smena body kassa_service.SmenaCreate true "CreateSmenaRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateSmena(ctx *gin.Context) {
	var Smena kassa_service.SmenaCreate

	err := ctx.ShouldBind(&Smena)
	if err != nil {
		h.handlerResponse(ctx, "CreateSmena", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SmenaService().Create(ctx, &Smena)
	if err != nil {
		h.handlerResponse(ctx, "SmenaService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Smena response", http.StatusOK, resp)
}

// GetByIdSmena godoc
// @ID get-by-id_smena
// @Router /smena/{id} [GET]
// @Summary Get By ID Smena
// @Description Get By ID Smena
// @Tags Smena
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdSmena(ctx *gin.Context) {
	SmenaId := ctx.Param("id")

	if !helper.IsValidUUID(SmenaId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.SmenaService().GetById(ctx, &kassa_service.SmenaPrimaryKey{Id: SmenaId})
	if err != nil {
		h.handlerResponse(ctx, "SmenaService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Smena response", http.StatusCreated, resp)
}

// SmenaGetList godoc
// @ID get-list-smena
// @Router /smena [GET]
// @Summary Get List Smena
// @Description Get List Smena
// @Tags Smena
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-branch query string false "search-by-branch"
// @Param search-by-employee query string false "search-by-employee"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SmenaGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Smena offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Smena limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.SmenaService().GetList(ctx.Request.Context(), &kassa_service.SmenaGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByBranch:   ctx.Query("search-by-branch"),
		SearchByEmployee: ctx.Query("search-by-employee"),
	})
	if err != nil {
		h.handlerResponse(ctx, "SmenaService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Smena response", http.StatusOK, resp)
}

// SmenaUpdate godoc
// @ID update-smena
// @Router /smena/{id} [PUT]
// @Summary Update Smena
// @Description Update Smena
// @Tags Smena
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Smena body kassa_service.SmenaUpdate true "UpdateSmenaRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SmenaUpdate(ctx *gin.Context) {
	var (
		id          = ctx.Param("id")
		SmenaUpdate kassa_service.SmenaUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&SmenaUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Smena should bind json", http.StatusBadRequest, err.Error())
		return
	}

	SmenaUpdate.Id = id
	resp, err := h.services.SmenaService().Update(ctx.Request.Context(), &SmenaUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.SmenaService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Smena response", http.StatusAccepted, resp)
}

// DeleteSmena godoc
// @ID delete-smena
// @Router /smena/{id} [DELETE]
// @Summary Delete Smena
// @Description Delete Smena
// @Tags Smena
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteSmena(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.SmenaService().Delete(c.Request.Context(), &kassa_service.SmenaPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.SmenaService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Smena response", http.StatusNoContent, resp)
}
