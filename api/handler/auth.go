package handler

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/GoFinalProject/project_api/genproto/organization_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
)

// Login godoc
// @ID login
// @Router /login [POST]
// @Summary Login
// @Description Login
// @Tags Login
// @Accept json
// @Procedure json
// @Param login body organization_service.EmployeeLogin true "LoginRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) Login(ctx *gin.Context) {
	var login organization_service.Employee

	err := ctx.ShouldBindJSON(&login) // parse req body to given type struct
	if err != nil {
		h.handlerResponse(ctx, "create staff", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.EmployeeService().GetById(ctx, &organization_service.EmployeePrimaryKey{Login: login.Login})
	if err != nil {
		if err.Error() == "no rows in result set" {
			h.handlerResponse(ctx, "staff does not exist", http.StatusBadRequest, "User does not exist")
			return
		}
		h.handlerResponse(ctx, "storage.staff.getByID", http.StatusInternalServerError, err.Error())
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(resp.Password), []byte(login.Password))
	if err != nil {
		h.handlerResponse(ctx, "Compare Password", http.StatusInternalServerError, err.Error())
		return
	}

	token, err := helper.GenerateJWT(map[string]interface{}{
		"user_id": resp.Id,
	}, time.Hour, h.cfg.SecretKey)
	if err != nil {
		h.handlerResponse(ctx, "Generate JWT", http.StatusInternalServerError, err.Error())
		return
	}

	ctx.SetCookie("TOKEN", token, 1000, "/", "localhost", false, true)
	h.handlerResponse(ctx, "token", http.StatusAccepted, token)
}

// Register godoc
// @ID Register
// @Router /register [POST]
// @Summary Register
// @Description Register
// @Tags Register
// @Accept json
// @Procedure json
// @Param register body organization_service.EmployeeRegister true "RegisterRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) Register(ctx *gin.Context) {

	var createEmployee organization_service.EmployeeCreate

	err := ctx.ShouldBindJSON(&createEmployee)
	if err != nil {
		h.handlerResponse(ctx, "error user should bind json", http.StatusBadRequest, err.Error())
		return
	}

	if len(createEmployee.Password) < 6 {
		h.handlerResponse(ctx, "Password should include more than 6 characters", http.StatusBadRequest, errors.New("!Password len should inculude more than 8 elements"))
		return
	}

	_, err = h.services.EmployeeService().GetById(ctx, &organization_service.EmployeePrimaryKey{
		Login: createEmployee.Login,
	})
	if err != nil {
		if err.Error() == "rpc error: code = InvalidArgument desc = no rows in result set" {
			resp, err := h.services.EmployeeService().Create(ctx, &createEmployee)
			if err != nil {
				h.handlerResponse(ctx, "Register h.services.EmployeeService().Create", http.StatusInternalServerError, err.Error())
				return
			}
			h.handlerResponse(ctx, "Register Response", http.StatusCreated, resp)
			return
		} else {
			fmt.Println(err.Error())
			h.handlerResponse(ctx, "Register h.services.EmployeeService().GetById", http.StatusBadRequest, err.Error())
			return
		}
	} else if err == nil {
		h.handlerResponse(ctx, "User already exist, please login", http.StatusBadRequest, nil)
		return
	}

}
