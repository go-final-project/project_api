package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/kassa_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateSaleProduct godoc
// @ID create-sale-product
// @Router /sale-product [POST]
// @Summary Create SaleProduct
// @Description Create SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param SaleProduct body kassa_service.SaleProductCreate true "CreateSaleProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateSaleProduct(ctx *gin.Context) {
	var SaleProduct kassa_service.SaleProductCreate

	err := ctx.ShouldBind(&SaleProduct)
	if err != nil {
		h.handlerResponse(ctx, "CreateSaleProduct", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleProductService().Create(ctx, &SaleProduct)
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create SaleProduct response", http.StatusOK, resp)
}

// GetByIdSaleProduct godoc
// @ID get-by-id_sale-product
// @Router /sale-product/{id} [GET]
// @Summary Get By ID SaleProduct
// @Description Get By ID SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdSaleProduct(ctx *gin.Context) {
	SaleProductId := ctx.Param("id")

	if !helper.IsValidUUID(SaleProductId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.SaleProductService().GetById(ctx, &kassa_service.SaleProductPrimaryKey{Id: SaleProductId})
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id SaleProduct response", http.StatusCreated, resp)
}

// SaleProductGetList godoc
// @ID get-list-sale-product
// @Router /sale-product [GET]
// @Summary Get List SaleProduct
// @Description Get List SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-category query string false "search-by-category"
// @Param search-by-barcode query string false "search-by-barcode"
// @Param search-by-brand query string false "search-by-brand"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleProductGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list SaleProduct offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list SaleProduct limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.SaleProductService().GetList(ctx.Request.Context(), &kassa_service.SaleProductGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByCategory: ctx.Query("search-by-category"),
		SearchByBarcode:  ctx.Query("search-by-barcode"),
		SearchByBrand:    ctx.Query("search-by-brand"),
	})
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list SaleProduct response", http.StatusOK, resp)
}

// SaleProductUpdate godoc
// @ID update-sale-product
// @Router /sale-product/{id} [PUT]
// @Summary Update SaleProduct
// @Description Update SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param SaleProduct body kassa_service.SaleProductUpdate true "UpdateSaleProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleProductUpdate(ctx *gin.Context) {
	var (
		id                = ctx.Param("id")
		SaleProductUpdate kassa_service.SaleProductUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&SaleProductUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error SaleProduct should bind json", http.StatusBadRequest, err.Error())
		return
	}

	SaleProductUpdate.Id = id
	resp, err := h.services.SaleProductService().Update(ctx.Request.Context(), &SaleProductUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.SaleProductService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create SaleProduct response", http.StatusAccepted, resp)
}

// DeleteSaleProduct godoc
// @ID delete-sale-product
// @Router /sale-product/{id} [DELETE]
// @Summary Delete SaleProduct
// @Description Delete SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteSaleProduct(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.SaleProductService().Delete(c.Request.Context(), &kassa_service.SaleProductPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.SaleProductService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create SaleProduct response", http.StatusNoContent, resp)
}
