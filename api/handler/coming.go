package handler

import (
	"gitlab.com/GoFinalProject/project_api/genproto/stock_service"
	"gitlab.com/GoFinalProject/project_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateComing godoc
// @ID create-coming
// @Router /coming [POST]
// @Summary Create Coming
// @Description Create Coming
// @Tags Coming
// @Accept json
// @Procedure json
// @Param Coming body stock_service.ComingCreate true "CreateComingRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateComing(ctx *gin.Context) {
	var coming stock_service.ComingCreate

	err := ctx.ShouldBind(&coming)
	if err != nil {
		h.handlerResponse(ctx, "CreateComing", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ComingService().Create(ctx, &coming)
	if err != nil {
		h.handlerResponse(ctx, "ComingService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Coming response", http.StatusCreated, resp)
}

// GetByIdComing godoc
// @ID get-by-id-coming
// @Router /coming/{id} [GET]
// @Summary Get By ID Coming
// @Description Get By ID Coming
// @Tags Coming
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdComing(ctx *gin.Context) {
	comingId := ctx.Param("id")

	if !helper.IsValidUUID(comingId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.ComingService().GetById(ctx, &stock_service.ComingPrimaryKey{Id: comingId})
	if err != nil {
		h.handlerResponse(ctx, "ComingService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Coming response", http.StatusOK, resp)
}

// ComingGetList godoc
// @ID get-list-coming
// @Router /coming [GET]
// @Summary Get List Coming
// @Description Get List Coming
// @Tags Coming
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-branch query string false "search-by-branch"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ComingGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Coming offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Coming limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.ComingService().GetList(ctx.Request.Context(), &stock_service.ComingGetListRequest{
		Offset:         int64(offset),
		Limit:          int64(limit),
		SearchByBranch: ctx.Query("search-by-branch"),
	})
	if err != nil {
		h.handlerResponse(ctx, "ComingService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Coming response", http.StatusOK, resp)
}

// ComingUpdate godoc
// @ID update-coming
// @Router /coming/{id} [PUT]
// @Summary Update Coming
// @Description Update Coming
// @Tags Coming
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Coming body stock_service.ComingUpdate true "UpdateComingRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ComingUpdate(ctx *gin.Context) {
	var (
		id           = ctx.Param("id")
		ComingUpdate stock_service.ComingUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&ComingUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Coming should bind json", http.StatusBadRequest, err.Error())
		return
	}

	ComingUpdate.Id = id
	resp, err := h.services.ComingService().Update(ctx.Request.Context(), &ComingUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.ComingService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Coming response", http.StatusAccepted, resp)
}

// DeleteComing godoc
// @ID delete-coming
// @Router /coming/{id} [DELETE]
// @Summary Delete Coming
// @Description Delete Coming
// @Tags Coming
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteComing(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.ComingService().Delete(c.Request.Context(), &stock_service.ComingPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.ComingService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Coming response", http.StatusNoContent, resp)
}
