package config

import (
	"fmt"
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"

	TimeExpiredAt = time.Hour * 720
)

type Config struct {
	ServiceName string
	Environment string // debug, test, release
	Version     string

	ServiceHost string
	HTTPPort    string
	HTTPScheme  string
	Domain      string

	DefaultOffset string
	DefaultLimit  string

	OrganizationServiceHost string
	OrganizationGRPCPort    string

	ProductServiceHost string
	ProductGRPCPort    string

	StockServiceHost string
	StockGRPCPort    string

	KassaServiceHost string
	KassaGRPCPort    string

	PostgresMaxConnections int32

	SecretKey string
}

// Load ...
func Load() Config {
	if err := godotenv.Load("./.env"); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}

	config.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "api_gateway_service"))
	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", ReleaseMode))
	config.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))

	config.ServiceHost = cast.ToString(getOrReturnDefaultValue("SERVICE_HOST", "localhost"))
	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8001"))
	config.HTTPScheme = cast.ToString(getOrReturnDefaultValue("HTTP_Scheme", "http"))
	config.Domain = cast.ToString(getOrReturnDefaultValue("DOMAIN", "localhost:8001"))

	config.DefaultOffset = cast.ToString(getOrReturnDefaultValue("OFFSET", "0"))
	config.DefaultLimit = cast.ToString(getOrReturnDefaultValue("LIMIT", "10"))

	config.OrganizationServiceHost = cast.ToString(getOrReturnDefaultValue("ORGANIZATION_SERVICE_HOST", "localhost"))
	config.OrganizationGRPCPort = cast.ToString(getOrReturnDefaultValue("ORGANIZATION_GRPC_PORT", ":9101"))

	config.ProductServiceHost = cast.ToString(getOrReturnDefaultValue("PRODUCT_SERVICE_HOST", "localhost"))
	config.ProductGRPCPort = cast.ToString(getOrReturnDefaultValue("PRODUCT_GRPC_PORT", ":9102"))

	config.StockServiceHost = cast.ToString(getOrReturnDefaultValue("STOCK_SERVICE_HOST", "localhost"))
	config.StockGRPCPort = cast.ToString(getOrReturnDefaultValue("STOCK_GRPC_PORT", ":9103"))

	config.KassaServiceHost = cast.ToString(getOrReturnDefaultValue("KASSA_SERVICE_HOST", "localhost"))
	config.KassaGRPCPort = cast.ToString(getOrReturnDefaultValue("KASSA_GRPC_PORT", ":9104"))

	config.SecretKey = cast.ToString(getOrReturnDefaultValue("SECRET_KEY", "secret"))

	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
